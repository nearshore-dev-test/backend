import ExpressServer from './server/ExpressServer';

const port = process.env.PORT ? +process.env.PORT : 4000;
const server = new ExpressServer();

server.runOn(port);
