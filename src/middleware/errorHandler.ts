import { Request, Response } from 'express';

export function errorHandler(error: any, request: Request, response: Response, next: (err?: any) => any): any {
    
    error.message ? response.status(error.status).json({code: error.customCode, message: error.message})
        : response.status(error.status).json({});
}