import 'reflect-metadata';
import express, { Application } from 'express';
import http, { Server } from 'http';
import { useExpressServer } from 'routing-controllers';
import { IServer } from './definitions/IServer';
import LoanController from '../controllers/LoanController';
import PaymentController from '../controllers/PaymentController';
import InformationController from '../controllers/InformationController';
import cors from 'cors';
import morgan from 'morgan';
export default class ExpressServer implements IServer{
    readonly app: Application;
    private server: Server;
    
    constructor(){
        this.app = express();
        this.app.use(express.json());
        this.app.use(cors());
        this.app.use(morgan('tiny'));
        this.app = useExpressServer(this.app, {
            routePrefix: '',
            defaultErrorHandler: false,
            controllers: [LoanController, PaymentController, InformationController]
        });        
    }

    runOn(port: number): void {
        this.server = http.createServer(this.app);
        this.server.listen(port, ()=>console.log('Listening on port ', port));
    }
}