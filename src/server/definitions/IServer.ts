export interface IServer {
    runOn(port: number): void;
}
