import { ICustomer } from '../../models/ICustomer';

export interface IPaymentService {
    pay(customer: ICustomer): Promise<ICustomer>;
}
