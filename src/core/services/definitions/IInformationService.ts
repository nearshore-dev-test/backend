import { ICustomer } from '../../models/ICustomer';

export interface IInformationService {
    getInformation(customer: ICustomer): Promise<ICustomer>;
}
