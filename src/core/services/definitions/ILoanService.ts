import { ICustomer } from '../../models/ICustomer';

export interface ILoanService {
    requestLoan(customer: ICustomer): Promise<ICustomer>;
}
