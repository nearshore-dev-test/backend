import { ICustomer } from '../models/ICustomer';
import { IInformationService } from './definitions/IInformationService';
import LoanDao from '../../dao/LoanDao';
import NotFoundError from '../../errors/NotFoundError';

export default class InformationService implements IInformationService{
    readonly loanDao = LoanDao.getInstance();

    async getInformation(customer: ICustomer): Promise<ICustomer> {
        const customerInDb = await this.loanDao.findByEmail(customer.email);

        if(customerInDb == null) throw new NotFoundError('');
        return customerInDb;
    }

}