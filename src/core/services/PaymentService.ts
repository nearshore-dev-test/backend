import { ICustomer } from '../models/ICustomer';
import { IPaymentService } from './definitions/IPaymentService';
import LoanDao from '../../dao/LoanDao';
import NotFoundError from '../../errors/NotFoundError';
import ForbiddenError from '../../errors/ForbiddenError';

export default class PaymentService implements IPaymentService{
    readonly loanDao = LoanDao.getInstance();

    async pay(customer: ICustomer): Promise<ICustomer> {
        const customerInDb = await this.loanDao.findByEmail(customer.email);
        if(!customerInDb) throw new NotFoundError('Resource not found');
        
        const pay = <number>customer.amount;
        const debt = <number>customerInDb.amount;
        
        if (debt === 0) throw new ForbiddenError('Cannot accept, debt is zero ', 101);
        if ( pay > debt) throw new ForbiddenError('Cannot accept, payment is greater than debt ', 100);
        
        const newAmount = debt - pay;
        return await this.loanDao.updateAmount(customer.email, newAmount);
    }

}