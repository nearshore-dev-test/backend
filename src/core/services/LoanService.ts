import { ICustomer } from '../models/ICustomer';
import { ILoanService } from './definitions/ILoanService';
import LoanDao from '../../dao/LoanDao';
import ForbiddenError from '../../errors/ForbiddenError';

export default class LoanService implements ILoanService{
    readonly loanDao: LoanDao = LoanDao.getInstance();
    private readonly maxLoan: number = 1000;

    async requestLoan(customer: ICustomer): Promise<ICustomer> {
        const customerInDb = await this.loanDao.findByEmail(customer.email)!;
        
        if(!customerInDb) {            
            if(customer.amount! > 50) throw new ForbiddenError('initial loan must be less than 50$', 101);
            return await this.loanDao.createLoan(customer.email, customer.amount!);            
        }

        const newLoanQuantity = +customer.amount! + +customerInDb.amount!;        
        if (newLoanQuantity > this.maxLoan) throw new ForbiddenError('total loan must be less than 1000$', 100);
        
        return await this.loanDao.updateAmount(customer.email, newLoanQuantity);
    }    
}
