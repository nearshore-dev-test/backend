import { JsonController, Post, Req, Res, UseAfter } from 'routing-controllers';
import { Request, Response, NextFunction } from 'express';
import { ICustomer } from '../core/models/ICustomer';
import PaymentService from '../core/services/PaymentService';
import { errorHandler } from '../middleware/errorHandler';

@JsonController()
export default class PaymentController {
    private readonly paymentService = new PaymentService();

    @Post('/payment')
    @UseAfter(errorHandler)
    async pay(@Req() request: Request, @Res() response: Response, next: NextFunction): Promise<ICustomer | undefined>{
        try {
            return this.paymentService.pay(request.body);            
        } catch (ex) {
            next(ex);
        }
    }
}