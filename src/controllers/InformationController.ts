import { Req, Res, Post, JsonController, HttpCode, UseAfter } from 'routing-controllers';
import { ICustomer } from '../core/models/ICustomer';
import { Request, Response, NextFunction } from 'express';
import { errorHandler } from '../middleware/errorHandler';
import InformationService from '../core/services/InformationService';

@JsonController()
export default class InformationController{
    private readonly informationService = new InformationService();

    @Post('/information')
    @UseAfter(errorHandler)
    async getInformation(@Req() request: Request, @Res() response: Response, next: NextFunction): Promise<ICustomer | undefined>{
        try {
            return this.informationService.getInformation(request.body);
        } catch (ex) {
            next(ex);
        }
    }
}