import {
    Post,
    Body,
    HttpCode,
    JsonController,
    Req,
    Res,
    UseAfter,
} from 'routing-controllers';
import LoanService from '../core/services/LoanService';
import { ICustomer } from '../core/models/ICustomer';
import { ILoanService } from '../core/services/definitions/ILoanService';
import { errorHandler } from '../middleware/errorHandler';

@JsonController()
export default class LoanController {
    private readonly service: ILoanService = new LoanService();

    @HttpCode(201)
    @Post('/loan')
    @UseAfter(errorHandler)
    async requestLoan(@Req() request: any, @Res() response: any, next: any): Promise<ICustomer | undefined> {
        try {
            return this.service.requestLoan(request.body);
        } catch (ex) {
            next(ex);
        }
    }
}
