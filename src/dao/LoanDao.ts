import { ICustomer } from '../core/models/ICustomer';
import { ILoanDao } from './definitions/ILoanDao';

export default class LoanDao implements ILoanDao {
    private static instance: LoanDao;
    readonly persistence: ICustomer[] = [];

    private constructor (){ }

    static getInstance() : LoanDao{
        if (LoanDao.instance == null) LoanDao.instance = new LoanDao();
        return LoanDao.instance;
    }

    async findByEmail(email: string): Promise<ICustomer | undefined> {
        const customer = this.persistence.find(
            (customer) => customer.email === email
        );
        return Promise.resolve(customer);
    }

    async createLoan(email: string, amount: number): Promise<ICustomer> {
        this.persistence.push({ email, amount });

        return Promise.resolve(this.persistence[this.persistence.length - 1]);
    }

    async updateAmount(email: string, amount: number): Promise<ICustomer> {
        const index = this.persistence.findIndex(
            (customer) => customer.email === email
        );

        this.persistence[index].amount = amount;
        return Promise.resolve(this.persistence[index]);
    }
}
