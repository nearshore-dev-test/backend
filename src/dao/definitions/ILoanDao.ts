import { ICustomer } from '../../core/models/ICustomer';

export interface ILoanDao {
    findByEmail(email: string): Promise<ICustomer | undefined>;
    updateAmount(email: string, amount: number): Promise<ICustomer>;
    createLoan(email: string, amount: number): Promise<ICustomer>;
}
