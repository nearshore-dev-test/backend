export default class ForbiddenError extends Error {
    customCode: number;
    status = 403;

    constructor(message:string, customCode:number) {
        super(message);
        this.name = 'CustomError';
        this.customCode = customCode;
        Error.captureStackTrace(this, ForbiddenError);
    }
}
