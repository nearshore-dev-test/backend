export default class NotFoundError extends Error {
    readonly status = 404;

    constructor(message:string) {
        super(message);
        this.name = 'CustomError';
        Error.captureStackTrace(this, NotFoundError);
    }
}