import PaymentService from '../../../src/core/services/PaymentService';
import LoanService from '../../../src/core/services/LoanService';
import { ICustomer } from '../../../src/core/models/ICustomer';
import NotFoundError from '../../../src/errors/NotFoundError';
import ForbiddenError from '../../../src/errors/ForbiddenError';

describe('Test Loan Service', () => {
    const paymentService = new PaymentService();
    const loanService = new LoanService();
    const customer: ICustomer = {email: 'user@mail.com', amount: 0};

    afterEach(()=>{
        paymentService.loanDao.persistence.length = 0;
    });
    it('shoud throw a NotFoundError when customer is not in the DB', async()=>{
        expect.assertions(1);

        try {            
            await paymentService.pay(customer);
        } catch (error) {
            expect(error).toBeInstanceOf(NotFoundError);
        }
    });

    it('shoud throw a ForbiddenError with customCode 101 when debt is 0 ', async()=>{
        expect.assertions(3);
        await loanService.requestLoan(customer);

        try {            
            await paymentService.pay(customer);
        } catch (error) {
            expect(error).toBeInstanceOf(ForbiddenError);
            expect(error.message).toContain('debt is zero');
            expect(error.customCode).toBe(101);
        }
    });

    it('shoud throw a ForbiddenError with customCode 100 when the payment is greater than the debt ', async()=>{
        expect.assertions(3);
        await loanService.requestLoan({...customer, amount:40});

        try {            
            await paymentService.pay({...customer, amount:100});
        } catch (error) {
            expect(error).toBeInstanceOf(ForbiddenError);
            expect(error.message).toContain('payment is greater than debt');
            expect(error.customCode).toBe(100);
        }
    });

    it('shoud decrement amount when debt is not 0, and payment is less than debt ', async()=>{
        expect.assertions(1);
        const initialAmount = 40;
        await loanService.requestLoan({...customer, amount:initialAmount});

        const payment = 10;
        const response = await paymentService.pay({...customer, amount:payment});

        const updatedAmount = initialAmount - payment;
        expect(response).toMatchObject({email: customer.email, amount: updatedAmount});

    });

});