import LoanService from '../../../src/core/services/LoanService';
import { ICustomer } from '../../../src/core/models/ICustomer';
import NotFoundError from '../../../src/errors/NotFoundError';
import InformationService from '../../../src/core/services/InformationService';
import { info } from 'node:console';

describe('Test Information Service', () => {
    const informationService = new InformationService();
    const loanService = new LoanService();
    const customer: ICustomer = {email: 'user@mail.com'};

    afterEach(()=>{
        informationService.loanDao.persistence.length = 0;
    });

    it('should throw a NotFoundError if the customer isn\'t in the DB', async ()=>{
        expect.assertions(1);

        try {
            await informationService.getInformation(customer);
        } catch (error) {
            expect(error).toBeInstanceOf(NotFoundError);
        }        
    });

    it('should return the current state of the Customer when is in the DB', async ()=>{
        expect.assertions(1);
        customer.amount = 50;
        await loanService.requestLoan(customer);

        const response = await informationService.getInformation(customer);

        expect(response).toMatchObject(customer);
    });
});