import LoanService from '../../../src/core/services/LoanService';
import { ICustomer } from '../../../src/core/models/ICustomer';

describe('Test Loan Service', () => {
    const loanService = new LoanService();

    afterEach(()=>{
        loanService.loanDao.persistence.length = 0;
    });

    it('Should return a new customer object when loan is less than 50$', async () => {
        expect.assertions(1);

        const newCustomer = <ICustomer>{email: 'user@mail.com', amount:50};
        
        const response = await loanService.requestLoan(newCustomer);

        expect(response).toMatchObject(newCustomer);
    });

    it('Should throw an error when new loan is greater than 50$', async () => {
        const newCustomer = <ICustomer>{email: 'user@mail.com', amount:510};      

        try{            
            await loanService.requestLoan(newCustomer);
            
            expect(loanService.requestLoan).toThrow('initial loan must be less than 50$');
        } catch(ex){
            null;
        }
    });

    it('Should throw an error when total Loan is greater than 1000$', async () => {
        const newCustomer = <ICustomer>{email: 'user@mail.com', amount:20};      
        await loanService.requestLoan(newCustomer);
        newCustomer.amount = 1000;

        try{            
            await loanService.requestLoan(newCustomer);
            
            expect(loanService.requestLoan).toThrow('total loan must be less than 1000$');
        } catch(ex){
            null;
        }
    });

    it('Should return the current amount when it is less than 1000', async () => {
        const initialAmount = 20;
        const customer = <ICustomer>{email: 'user@mail.com', amount:initialAmount};      
        await loanService.requestLoan(customer);
        
        const newAmount = 980;
        customer.amount = newAmount;
        const response = await loanService.requestLoan(customer);
        
        expect(response).toMatchObject({...customer, amount: initialAmount+newAmount});
    });
});
