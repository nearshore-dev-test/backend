import LoanDao from '../../src/dao/LoanDao';

describe('Test Loan Dao', () => {
    const loanDao = LoanDao.getInstance();
    const email = 'user@mail.com';
    const amount = 50;

    afterEach(()=>{
        loanDao.persistence.length = 0;
    });

    it('should return undefined, when customer is not saved in the DB', async ()=>{
        expect.assertions(1);
        const email = 'user@mail.com';

        const response = await loanDao.findByEmail(email);

        expect(response).toBeUndefined();
    });

    it('should create a new customer', async()=>{
        expect.assertions(1);        

        const response = await loanDao.createLoan(email, amount);

        expect(response).toMatchObject({email, amount});
    });

    it('should update the amount in a given Customer', async()=>{
        expect.assertions(1);
        await loanDao.createLoan(email, amount);

        const updatedAmount = 100;
        const response = await loanDao.updateAmount(email, updatedAmount);

        expect(response).toMatchObject({email, amount: updatedAmount});
    });

});