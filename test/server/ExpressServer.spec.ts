import {IServer} from '../../src/server/definitions/IServer';
import ExpressServer from '../../src/server/ExpressServer';

describe('Server test', ()=>{
    it('should create a Expres Server', ()=>{
        const server: IServer = new ExpressServer();
        expect(server).toBeDefined();
    });
});