import supertest from 'supertest';
import ExpressServer from '../../src/server/ExpressServer';

const app = new ExpressServer().app;
const request = supertest(app);
describe('POST /payment', ()=>{
    it('should return a respond with a 404 Http Code when a new customer requests for payment', async()=>{
        const response = await request.post('/payment');
        expect(response.status).toBe(404);
    });
});