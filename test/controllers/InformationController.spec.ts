import supertest from 'supertest';
import ExpressServer from '../../src/server/ExpressServer';

const app = new ExpressServer().app;
const request = supertest(app);
describe('POST /information', ()=>{
    it('should return a body in the response', async()=>{
        const response = await request.post('/information');

        expect(response.body).toBeDefined();
    });
});
