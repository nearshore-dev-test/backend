import supertest from 'supertest';
import ExpressServer from '../../src/server/ExpressServer';

const app = new ExpressServer().app;
const request = supertest(app);
describe('POST /loan', ()=>{
    it('should return a respond with a 201 Http Code', async()=>{
        const response = await request.post('/loan');
        expect(response.status).toBe(201);
    });
});
