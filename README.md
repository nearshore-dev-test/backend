**Instructions**
- Clone the repository:
    `git clone https://gitlab.com/nearshore-dev-test/backend.git`

- Install the dependencies:
    `cd backend && yarn install` or `cd backend && npm install`

- The PORT by default is 4000, but you can define a env var for that variable in the command line.

- Start the project:

    ` yarn start` or `npm start`

- You can perform requests to the next endpoints (by default in http://localhost:4000):

| Endpoint | body |Responses
| ------ | ------ |------|
| /loan | {"email": string, "amount": number} | 201 : success|
| | |403 forbidden operation |
| /payment| {"email": string, "amount": number}|200: success |
| | |403: forbidden operation|
|  /information| {"email": string} |200 : success |
| | |404: resource nout found|
